#ifndef __NETWORK_H_
#define __NETWORK_H_

#include <vector>

#include "Layer.h"

class Network
{
public:
    Network(const int& n_inputs, const int& n_outputs, const int& n_hidden = 0, const double& learningRate = 0.3);
    Network(const std::vector<int>& layout, const double& learningRate = 0.3);

    Matrix<> getOutput() const;

    void setLearningRate(const double& val);
    void setHiddenActivation(const std::string& actType);

    // Return the prediction of the model
    [[nodiscard]] Matrix<> predict(const Matrix<>& input) const;

    void fit(const Matrix<>& inputs, const Matrix<>& labels);
    double evaluate( const Matrix<>& inputs, const Matrix<>& labels) const;



private:
    std::vector<Layer> m_Layers;
    double m_LearningRate;

    void m_ForwardPass(const Matrix<>& input );
    // Will return the loss
    double m_BackPropagation(const Matrix<>& input, const Matrix<>& label);

    double m_Loss( const Matrix<>& error ) const;
};

#endif