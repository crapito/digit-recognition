#include <iostream>

#include "../Matrix.h"


int main()
{

    // Initialize Matrix from data
    float data[3][4] = {{1,2,3,4},{5,6,7,8}, {-2,0.5f, 4,4}};
    Matrix<float> A(data[0], 3, 4); // shape: 3x4
    std::cout << "Matrix A:\n";
    A.print();

    // Initialize Matrix with specified value
    Matrix<float> B(10.0f, 4, 2);   // shape: 4x2
    std::cout << "\nMatrix B:\n";
    B.print();

    // Initialize random matrix (values in [0,1])
    Matrix<float> R = Matrix<float>::Random(2,3); // shape: 2x3
    std::cout << "\nMatrix R:\n";
    R.print();

    // Extract the diagonal of the matrix A
    Matrix<float> vec = A.getDiag();    // shape: 1x3
    std::cout << "\nVector containing diagonal of A:\n";
    vec.print();

    // Recreate a diagonal matrix from this vector
    Matrix<float> D = Matrix<float>::Diag(vec); // shape: 3x3
    std::cout << "\nDiagonal matrix D:\n";
    D.print();

    // Compute (BR - A^T) * 10
    std::cout << "\n(BR - A^T) * 10:\n";
    Matrix<float> result = (B.dot(R) - A.transpose()) * 10.0f;
    result.print();

    // Take the last result and transform it according to:
    // r_{i,j} -> round( r_{i,j} / (1 + i*i + j) )
    std::cout << "\nApply the elementwise transformation: round( r_{i,j} / (1 + i*i + j) ):\n";
    result.elementwise([](float e, int i, int j) -> float {
        return round( e / (1 + i*i + j) );
    });
    result.print();

    return EXIT_SUCCESS;
}