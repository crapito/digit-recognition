#include <iostream>
#include <random>
#include <vector>
#include <math.h>

#include "Network.h"


#define TRAIN_SIZE 1000
#define TEST_SIZE 5000

void testNetwork( Network& net );
void genData(Matrix<>& data, Matrix<>& label, const int& size);


int main()
{

    const std::vector<int> layout = {3, 3, 1};

    Network net(layout);
    net.setHiddenActivation("relu");
    testNetwork( net );

    std::cout << "\n\n------------------------------------------------\n\n";

    // Training the model
    Matrix<> train_x, train_y;
    genData(train_x, train_y, TRAIN_SIZE);
    net.fit(train_x, train_y);

    std::cout << "\n\n------------------------------------------------\n\n";
    testNetwork( net );

    

    return EXIT_SUCCESS;
}


void genData(Matrix<>& data, Matrix<>& label, const int& size)
{
    data = Matrix<>(3, size);
    label = Matrix<>(1, size);


    static std::random_device rd;
    static std::mt19937 tw(rd());
    static std::uniform_real_distribution<double> dist(0, 1);


    for (int i = 0; i < size; ++i)
    {
        double X[3] = { dist(tw), dist(tw), dist(tw)};

        // -> we want to train to get our network to x + y + z - 1
        double L = X[0] + X[1] + X[2] - 1;

        data(0, i) = X[0];
        data(1, i) = X[1];
        data(2, i) = X[2];
        label(0, i) = L;
    }
}

void testNetwork( Network& net )
{
    std::cout << "Testing network:\n";

    double loss = 0.0;
    Matrix<> test_x, test_y;
    genData(test_x, test_y, TEST_SIZE);

    loss = net.evaluate( test_x, test_y );
    std::cout << "The total loss on the test sample is: " << loss << '\n';
    std::cout << "This amount to an average loss of: " << loss / (double)TEST_SIZE << '\n';

    std::cout << "Some sample results: \n";
    Matrix<> sample(3, 1);
    sample[0] = 0.0; sample[1] = 0.5; sample[2] = 0.2;
    std::cout << sample.transpose() << " -> " << net.predict(sample) <<
        " should be: " << sample.sum() - 1 << '\n';
    sample[0] = 0.0; sample[1] = 0.0; sample[2] = 0.0;
    std::cout << sample.transpose() << " -> " << net.predict(sample) <<
        " should be: " << sample.sum() - 1 << '\n';
    sample[0] = 1.0; sample[1] = -1.0; sample[2] = 0.5;
    std::cout << sample.transpose() << " -> " << net.predict(sample) <<
        " should be: " << sample.sum() - 1 << '\n';
    sample[0] = 0.6; sample[1] = 0.3; sample[2] = 0.1;
    std::cout << sample.transpose() << " -> " << net.predict(sample) <<
        " should be: " << sample.sum() - 1 << '\n';
}