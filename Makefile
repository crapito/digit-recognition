CC = g++

CXXFLAGS = -g -Wall -pedantic -std=c++17

PROJ := project

all: $(PROJ)


$(PROJ): main.o Network.o Layer.o
	$(CC) -o $@ $^


main.o: main.cpp Layer.h Matrix.h
	$(CC) $(CXXFLAGS) -c -o $@ $<

Network.o: Network.cpp Network.h Layer.o
	$(CC) $(CXXFLAGS) -c -o $@ $<

Layer.o: Layer.cpp Matrix.h Layer.h
	$(CC) $(CXXFLAGS) -c -o $@ $<

%.h:



# General Fallback Compilation rules
%.o: %.cpp
	$(CC) $(CXXFLAGS) -c -o $@ $<


clean:
	@rm -fv *.o $(PROJ) *.log